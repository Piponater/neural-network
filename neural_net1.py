#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 23 16:20:12 2018

@author: Pritesh
"""

import numpy as np # iniatialse numpy

#This is sigmoid activation function (Re-Lu is better)

# Computes sigmoid value
def sigmoid(x):
    return 1/(1+np.exp(-x))

# Computes sigmoid deriavtive value
def sigmoid_deriv(x): 
    x = 1/(1+np.exp(-x))
    return x*(1-x)

#A is image of + 
A=np.array([[0,1,0],
            [1,1,1],
            [0,1,0]])

#B is image of - 
B=np.array([[0,0,0],
            [1,1,1],
            [0,0,0]])

#C is an image of x
C=np.array([[1,0,1],
            [0,1,0],
            [1,0,1]])

#D is an image of x  
D=np.array([[1,1,1],
            [1,1,1],
            [1,1,1]])


a=np.array([[1,0,0,0]]).T
b=np.array([[0,1,0,0]]).T
c=np.array([[0,0,1,0]]).T
d=np.array([[0,0,0,1]]).T


images=[[A,a],[B,b],[C,c],[D,d]]

images=[[A,a],[B,b],[C,c],[D,d]]

# seed random numbers to make calculation deterministic (just a good practice) helps for debbuggin purposes!
np.random.seed(20)

#linear algebra nxm X mxp = nxp
#therefore we must specify the p diminsion as the number of
#neurons in the next layer
layer1=6
layer2=4
layer3=3

# initialize weights randomly between -0.5 and 0.5
weights1=np.random.uniform(0,1,(layer1,9)) - 0.5
weights2=np.random.uniform(0,1,(layer2,layer1)) - 0.5
weights3=np.random.uniform(0,1,(layer3,layer2)) - 0.5

bias=0.0
w1ij=weights1
w2ij=weights2

for epoche in range(0,10000):
    cost = 0 
    cost_der = 0
    grad_1 = 0
    grad_2 = 0
    
    for i in range(len(images)):
        #print(images[i][0])
        
        #Layer 0 input later input "layer 0"
        k = np.size(images[i][0])
        al0 = np.reshape(images[i][0], (k,1))
        #print(al0)
#        print(al0.shape, w1ij.shape)
        #Layer 1
        w1ij_al0 = np.dot(w1ij, al0) + bias
        al1 = sigmoid(w1ij_al0)
#        print(al1)
        
        #Layer 2
#        print(al1.shape, w2ij.shape)
        w2ij_al1 = np.dot(w2ij,al1) +  bias
        al2 = sigmoid(w2ij_al1)
#        print(al2)
        K = images[i][1]
        
        cost = cost + 0.5*(K - al2)**2
#        print(cost)        
        
        cost_der =  (K - al2)
        #print(cost_der)
        
        delta_2 = np.multiply(cost_der,sigmoid_deriv(al2))
#        print(delta_2)
        delta_1 = np.multiply((np.dot(w2ij.T,delta_2)),sigmoid_deriv(al1))
        
        grad_2 = grad_2 - np.dot(al1,delta_2.T) 
        grad_1 = grad_1 - np.dot(al0,delta_1.T) 
    
    w2ij = w2ij - 1*((grad_2.T)/3)
    w1ij = w1ij - 1*((grad_1.T)/3)
    if (epoche % 1000) == 0:
        print(epoche,sum(cost/3), np.sum(grad_2), np.sum(grad_1))
    #yplot.append(sum(cost/3))
    
for i in range(len(images)):
        #print(images[i][0])
        
        #Layer 0 input later input "layer 0"
        k = np.size(images[i][0])
        al0 = np.reshape(images[i][0], (k,1))
        #print(al0)
        
        #Layer 1
        w1ij_al0 = np.dot(w1ij, al0) + bias
        al1 = sigmoid(w1ij_al0)
        
        #Layer 2
        w2ij_al1 = np.dot(w2ij,al1) +  bias
        al2 = sigmoid(w2ij_al1)
        K = images[i][1]
        
        print("Cost")
        cost =  (K - al2)

        print("")
        #print(al1)
        
        print("Activare", i)
        print("cost")
        print(cost)
        print("al1")
        print(al1)
        print("al2")        
        print(np.round(al2, 3))
        print(images[i][1])
        



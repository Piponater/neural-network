#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 26 14:52:15 2018

@author: Pritesh
"""
import numpy as np
import timeit as ti


def sigmoid(x):
    return 1/(1 + np.exp(-x))

def sigmoid_deriv(x):
    return x*(1-x)

def array(x):
    k = np.size(x)
#    print(np.reshape(x, (k,1)))
    return np.reshape(x, (k,1))

def convert(images):
    images = list(map(array, images)) 
    return images
    
def initialise_weights(layers):
    weights = []
    for i in range(len(layers) -1):
#        print(layers[i])
        weights.append(2*np.random.random((layers[i],layers[i+1])) - 1)
    return weights   

def feed_forward(x,y):
    z = sigmoid(np.dot(x,y))
    return z

def forward_propagation(layers,batcharray,weights):
    activations = []
    for j in range(len(layers) - 1):
        if(j == 0):
            activations.append(feed_forward(batcharray,weights[j]))
        elif(j >= 1):
             activations.append(feed_forward(activations[j-1],weights[j]))
    return activations

def backward_propagation(target,activations,weights,batcharray):
    top = len(weights) - 1
    output_error = 0.0
    output_delta = 0.0
    for j in reversed(range(len(weights))):
        if(j == top):            
            output_error = target - activations[j]
            output_delta = output_error * sigmoid_deriv(activations[j])
            weights[j] = weights[j] + 1*np.dot(activations[j-1].T,output_delta)
        elif(0 < j):
            output_error = np.dot(output_delta, weights[j+1].T)
            output_delta = output_error*sigmoid_deriv(activations[j])
            weights[j] = weights[j] + 1*np.dot(activations[j+1].T,output_delta)
        elif(j == 0):
            output_error = np.dot(output_delta, weights[1].T)
            output_delta = output_error*sigmoid_deriv(activations[0])
            weights[0] = weights[0] + 1*np.dot(batcharray.T,output_delta)
    return weights

#-----------------deterministic
np.random.seed(1)
#-----------------deterministic


#Inputs Section-----------------------------|
#A is image of +
A=np.array([[0,1,0],
            [1,1,1],
            [0,1,0]])

#B is image of - 
B=np.array([[0,0,0],
            [1,1,1],
            [0,0,0]])

#C is an image of x
C=np.array([[1,0,1],
            [0,1,0],
            [1,0,1]])

#D is an image of x  
D=np.array([[1,1,1],
            [1,1,1],
            [1,1,1]])


images=[[A],[B],[C],[D]]
target=np.array([[1,0,1,0]]).T 
layers=[np.size(images[0]),6,3,1] # Layer structure, as many as desired and nodes 
convergence = 0.0001 #Error convergence
maxiterations=50000  #No of iterations of training network
#End of inputs Section----------------------------|

images = convert(images)
weights = initialise_weights(layers)
batcharray= np.concatenate((images), axis=1).T


for i in range(maxiterations):
    
    #forward_progations    
    activations = forward_propagation(layers,batcharray,weights)

    if(i % 10  == 0):
        print("Iterations",i,"Error:",(np.round(np.sum(target - activations[-1]),6)))
        if(np.sum(2*(target - activations[-1])**2) < convergence ):
            print("Error is less than {}".format(convergence))
            break
    if(i == maxiterations):
            "Print max cycle for training reached"
    #backward_propagation
    weights = backward_propagation(target,activations,weights,batcharray)


#Testing Network with All 4 inputs

print("")
print("Testing Network")

Output = forward_propagation(layers,batcharray,weights)
print(Output[-1])
    
    